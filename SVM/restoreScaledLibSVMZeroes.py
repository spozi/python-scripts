#This script is to restore back the zeroes that being deleted by LIBSVM svm-scale
#To use this script, the file is expected to be seperated with tab and end with "\r\n" not " \r\n"

import sys
import re

def file_len(fName):
    with open(fName) as f:
        for i, l in enumerate (f):
            pass
    f.close()
    return i + 1

class feature:
    key             = 0
    value           = 0
    
class instance:    
    def __init__(self, maxFeature):
        self.maxFeature      = maxFeature
        self.label           = 0 
        self.Features        = [feature() for j in xrange(self.maxFeature)]
    def update(self, line):
        data = line.split("\t")
        self.label = int(data[0])
        ctr = 1
        prev = 0
        ft = 0
        while ctr < len(data):                      #length of the line
            val = data[ctr].split(":")              #get the value feature
            ft = prev                               #get last value
            while ft < int(val[0]) - 1:
                self.Features[ft].key   = ft + 1
                self.Features[ft].value = 0
                ft += 1
            self.Features[ft].key   = int(val[0])
            self.Features[ft].value = float(val[1])
            ctr += 1
            prev = int(val[0])
        ft = prev
        while ft < self.maxFeature:
            self.Features[ft].key   = ft + 1
            self.Features[ft].value   = 0
            ft += 1
 
ifile   = sys.argv[1]				#input file
lenFile = file_len(ifile)
ofile   = "fixed_" + ifile		
nF      = int(sys.argv[2])			#number of features
print ofile

fileRead    = open(ifile, 'r')
fileWrite   = open(ofile, 'w') 

ctr_file_line = 0
for lines in fileRead.readlines():
    dataTrain = instance(nF)
    dataTrain.update(lines)
    newStr = str(dataTrain.label) + "\t"
    for csd in dataTrain.Features:
        newStr += str(csd.key)
        newStr += ":"
        newStr += str(csd.value)
        newStr += "\t"
    if(ctr_file_line == lenFile - 1):
        fileWrite.write(newStr + "\0")
    else:
        fileWrite.write(newStr + "\n")
    ctr_file_line += 1

fileRead.close()
fileWrite.close()