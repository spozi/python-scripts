#This file convert LIBSVMFormat to csv file

import sys
import re

fileRead    = open(sys.argv[1], 'r')
fileWrite   = open(sys.argv[1] + ".csv", 'w')
strType     = ""

for lines in fileRead.readlines():
    data = lines.split('\t')
    strType = str(data[0]) + ','
    ctr = 1
    while(ctr < len(data)-1):           #last is newline
        data2 = data[ctr].split(':')    
        if ctr == len(data) - 2:        
            strType += data2[1]
        else:
            strType += data2[1] + ','
        ctr += 1
    fileWrite.write(strType + '\n')
fileWrite.close()
fileRead.close()