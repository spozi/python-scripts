#This is to select each instance randomly and unique from train data

import sys
import random as rd

#function to check length of file
def file_len(fName):
    with open(fName) as f:
        for i, l in enumerate (f):
            pass
    f.close()
    return i + 1

#function to check unique number
def check_unique_number(list_number, num):
    for i in list_number:
        if num == i:
            return False                    #num is already inside list
    return True                             #num is unique

rd.seed()                                   #seed
fileRead            = sys.argv[1]
totalData           = int(sys.argv[2])      #total required data (instances) in a sample
totalSample         = int(sys.argv[3])      #total required sample
SampleType          = sys.argv[4]

outputSamplefile    = []
for sample_file_ctr in range(totalSample):
    outputSamplefile.append(SampleType  + "_sample_" + str(sample_file_ctr+1) + ".txt")

len_file            = file_len(fileRead)    #length of the original file
label_file          = 0                     #flag : to ensure data is balanced     
    
ifile               = open(fileRead, 'r')
linesString         = ifile.readlines()
ifile.close()

#outputting sample
UniqueSetListNumber = []
for sample_file_ctr in range(totalSample):
    ofile = open(outputSamplefile[sample_file_ctr], 'w')    #open file for output
    x = 0                                                   #set x (ctr) to 0                
    label_file = 0                                          #We only need totaldata for each sample
    while (x < totalData):
        randNum = rd.randint(0, len_file - 1)
        if check_unique_number(UniqueSetListNumber, randNum):       #if the random number is unique
            if label_file == linesString[randNum][0]:               #we do this checking to ensure that label data in each sample is balance
                continue
            else:
                label_file = linesString[randNum][0]                #record current label
                UniqueSetListNumber.append(randNum)                 #record the randNumber to the list
                if(x == totalData - 1):                             #if data now is totalData
                    endData = linesString[UniqueSetListNumber[-1]]
                    s = list(endData)
                    s[-1] = ""
                    ofile.write("".join(s))
                else:
                    ofile.write(linesString[UniqueSetListNumber[-1]])
                x += 1
    ofile.close()
    
num = 0
UniqueSetListNumber2 = []
while num < len(linesString):
    similar = 0
    for i in UniqueSetListNumber:
        if i == num:
            num += 1
            similar = 1
            break;
    if similar == 0:
        UniqueSetListNumber2.append(num)
        num += 1

ofile = open(SampleType + "_final_sample_.txt", 'w')
for i in UniqueSetListNumber2:
    ofile.write(linesString[i])
ofile.close

ofile = open("number", 'w')
num = 0
while num < len(UniqueSetListNumber):
    ofile.write(str(UniqueSetListNumber[num]) + "\n")
    num += 1
ofile.close()